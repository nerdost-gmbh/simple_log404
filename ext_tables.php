<?php

(static function() {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_simplelog404_domain_model_logentry', 'EXT:simple_log404/Resources/Private/Language/locallang_csh_tx_simplelog404_domain_model_logentry.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_simplelog404_domain_model_logentry');
})();
