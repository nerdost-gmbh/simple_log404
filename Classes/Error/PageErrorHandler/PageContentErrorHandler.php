<?php
namespace Nerdost\SimpleLog404\Error\PageErrorHandler;

use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Psr\Http\Message\ServerRequestInterface;
use Nerdost\SimpleLog404\Domain\Repository\LogEntryRepository;

class PageContentErrorHandler extends \TYPO3\CMS\Core\Error\PageErrorHandler\PageContentErrorHandler
{

    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param string $message
     * @param array $reasons
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function handlePageError(ServerRequestInterface $request, string $message, array $reasons = []): ResponseInterface
    {
        GeneralUtility::makeInstance(LogEntryRepository::class)->log(404, $request, $message);
        return parent::handlePageError($request, $message, $reasons);
    }

}
