<?php

declare(strict_types=1);

namespace Nerdost\SimpleLog404\Controller;

use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Template\ModuleTemplate;
use TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
use TYPO3\CMS\Core\Http\RedirectResponse;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Psr\Http\Message\ServerRequestInterface;


/**
 * This file is part of the "404 Logging" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Paul Beck <p.beck@nerdost.net>, Nerdost GmbH
 */

/**
 * LogEntryController
 */
class LogEntryController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{


    protected ?ModuleTemplate $moduleTemplate = null;

    protected ?PageRenderer $pageRenderer = null;

    /**
     * logEntryRepository
     *
     * @var \Nerdost\SimpleLog404\Domain\Repository\LogEntryRepository
     */
    protected $logEntryRepository = null;

    /**
     * @param \Nerdost\SimpleLog404\Domain\Repository\LogEntryRepository $logEntryRepository
     */
    public function injectLogEntryRepository(\Nerdost\SimpleLog404\Domain\Repository\LogEntryRepository $logEntryRepository)
    {
        $this->logEntryRepository = $logEntryRepository;
    }

    /**
     * @var \TYPO3\CMS\Backend\Template\ModuleTemplateFactory
     */
    protected $moduleTemplateFactory;

    /**
     * @param \TYPO3\CMS\Backend\Template\ModuleTemplateFactory $moduleTemplateFactory
     */
    public function __construct(
        ModuleTemplateFactory $moduleTemplateFactory
    ) {
        $this->moduleTemplateFactory = $moduleTemplateFactory;
    }

    /**
     * Set up the doc header properly here
     *
     * @param \TYPO3\CMS\Fluid\View\TemplateView $view
     * @return void
     */
    protected function initializeView(\TYPO3\CMS\Fluid\View\TemplateView $view)
    {
        $this->moduleTemplate = $this->moduleTemplateFactory->create($GLOBALS['TYPO3_REQUEST']);
        $this->moduleTemplate->setTitle('404 Log Overview');
        $this->moduleTemplate->setFlashMessageQueue($this->getFlashMessageQueue());
        $this->moduleTemplate->setModuleId('SimpleLog404Overview');
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \TYPO3\CMS\Backend\Routing\Exception\RouteNotFoundException
     */
    public function listAction()
    {
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);

        $qs = $this->logEntryRepository->createQuery()->getQuerySettings();
        $qs->setRespectStoragePage(false);
        $this->logEntryRepository->setDefaultQuerySettings($qs);

        $logEntries = $this->logEntryRepository->findAll()->toArray();

        $pm = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Package\PackageManager::class);

        $this->view->assignMultiple([
            'logEntries' => $logEntries,
            'redirectModuleActive' => $pm->isPackageActive('redirects')
        ]);

        $this->moduleTemplate->setContent($this->view->render());

        return $this->htmlResponse($this->moduleTemplate->renderContent());

    }


    /**
     * @param \Nerdost\SimpleLog404\Domain\Model\LogEntry $logEntry
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \TYPO3\CMS\Backend\Routing\Exception\RouteNotFoundException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function deleteAction(\Nerdost\SimpleLog404\Domain\Model\LogEntry $logEntry): ResponseInterface
    {
        $this->logEntryRepository->remove($logEntry);
        // Redirect back to module
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
        return new RedirectResponse($uriBuilder->buildUriFromRoutePath('/module/simple_log404/list'));
    }
}
