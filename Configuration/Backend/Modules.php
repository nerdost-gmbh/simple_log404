<?php

/**
 * Definitions for modules provided by EXT:simple_log404
 */
return [
    'site_simplelog404' => [
        'parent' => 'site',
        'access' => 'user',
        'icon' => 'EXT:simple_log404/Resources/Public/Icons/Extension.svg',
        'labels' => 'LLL:EXT:simple_log404/Resources/Private/Language/locallang_main.xlf',
        'extensionName' => 'SimpleLog404',
        'path' => '/module/simple_log404/list',
        'controllerActions' => [
            \Nerdost\SimpleLog404\Controller\LogEntryController::class => [
                'list', 'delete',
            ],
        ],
    ],
];
