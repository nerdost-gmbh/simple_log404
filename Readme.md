## How to use

Just install the extension and setup a default PageErrorHandler to display the content of your pages
404 Errorpage.

__Configuration__

Within the extension configurations you can change the amount of days until logs will be removed since they have last time been called.

This Extension will hook inside and log the requested failure URLs which will be displayed ordered
by their hitcount inside a new backend module.

Happy logging!

The development of this Extension is sponsored by https://nerdost.net - thanks!

Thanks to all contributors:

__Mike Street__ <mike@liquidlight.co.uk> who gave this extension some nice improvements and v11 compatibility
